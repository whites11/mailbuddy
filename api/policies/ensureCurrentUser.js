module.exports = (req, res, next) => {
  res.locals.current_user = false;
  return next();
};
