module.exports = (req, res, next) => {
  App.findOne({ id: req.param('id') }).populate('users').then((app) => {

    if (app) {
      // check if the current user is authorized to see this app
      if (_.includes(app.users.map(user => user.id), req.user.id)) {
        req.currentApp = app;

        return next();
      }
    }

    res.notFound();
  }).catch((err) => {
    throw err;
  });
};
