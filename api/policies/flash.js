/**
 * flash
 *
 * @module      :: Policy
 * @description :: Flash message handling policy
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = (req, res, next) => {

  res.locals.flash = {};
  res.locals.input = {};
  if (req.session.flash) {
    res.locals.flash = _.clone(req.session.flash);
  }
  if (req.session.input) {
    res.locals.input = _.clone(req.session.input);
  }

  req.session.flash = {};
  req.session.input = {};

  next();
};
