module.exports = (req, res, next) => {
  Email.findOne({ id: req.param('id') }).populate('attachments').then((email) => {

    if (email) {
      // check if the current user is authorized to see this app
      Membership.findOne({ appId: email.appId, userId: req.user.id }).exec((err, membership) => {
        if (err) throw err;

        if (membership) {
          req.currentEmail = email;
          return next();
        }

        res.notFound();
      });
    } else {
      res.notFound();
    }
  }).catch((err) => {
    throw err;
  });
};
