const gravatar = require('gravatar');

module.exports = (req, res, next) => {
  if (req.isSocket) {
    if (req.session &&
        req.session.passport &&
        req.session.passport.user) {
      // user is authenticated
      req.user = { id: req.session.passport.user };
      return next();
    }
    return res.json(401);
  }
  res.locals.userApps = [];
  // standard request
  if (req.isAuthenticated()) {
    const user = _.clone(req.user);
    user.avatar_url = gravatar.url(user.email, {s: 56})
    res.locals.current_user = user;

    // load user apps
    Membership.find({ userId: user.id }).populate('app').exec((err, memberships) => {
      if (err) throw err;

      res.locals.userApps = memberships
        .map((membership) => membership.app)
        .sort((a, b) => {return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1;});
    });

    return next();
  }
  return res.redirect('/login');
};
