module.exports = (req, res, next) => {
  User.findOne({ id: req.param('id') }).then((user) => {

    if (user) {
      req.selectedUser = user;
      return next();
    }

    res.notFound();
  }).catch((err) => {
    throw err;
  });
};
