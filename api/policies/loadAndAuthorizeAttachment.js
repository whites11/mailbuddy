module.exports = (req, res, next) => {
  Attachment.findOne({ id: req.param('id') }).populate('email').then((attachment) => {
    if (attachment) {
      // check if the current user is authorized to see this app
      Membership.findOne({ appId: attachment.email.app_id, userId: req.user.id }).exec((err, membership) => {
        if (err) throw err;

        if (membership) {
          req.currentAttachment = attachment;
          return next();
        }

        res.notFound();
      });
    } else {
      res.notFound();
    }
  }).catch((err) => {
    throw err;
  });
};
