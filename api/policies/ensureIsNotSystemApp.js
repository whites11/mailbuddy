module.exports = (req, res, next) => {
  if (req.currentApp.isSystem) {
    return res.forbidden();
  }

  next();
};
