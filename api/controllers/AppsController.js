/**
 * AppsController
 *
 * @description :: Server-side logic for managing emails
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const rand = require('random-key');
const bcrypt = require('bcrypt');

const generateApiKey = (callback) => {
  // generate a random string
  const key = rand.generateBase30(10);

  // ensure the random string is not currently an API key for any app
  App.findOne({ apiKey: key }).exec((err, record) => {
    if (err) {
      throw err;
    }

    if (record) {
      // a record was found, try again
      generateApiKey(callback);
    } else {
      // key is fine
      callback(key);
    }
  });
};

const generateApiSecret = (callback) => {
  const plainApiSecret = rand.generateBase30(10);
  bcrypt.hash(plainApiSecret, 10, (err, hash) => {
    if (err) {
      throw err;
    }

    callback(plainApiSecret, hash);
  });
};

module.exports = {
  index: (req, res) => {
    User.findOne({ id: req.user.id }).populate('apps').exec((err, user) => {
      if (err) throw err;

      res.locals.apps = user.apps.sort((a, b) => { return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1; });
      return res.view();
    });
  },

  new: (req, res) => res.view(),

  create: (req, res) => {
    const params = req.params.all();

    generateApiKey((apiKey) => {
      params.apiKey = apiKey;

      generateApiSecret((plainApiSecret, hash) => {
        params.apiSecret = hash;

        // create the APP record
        App.create(params, (createErr, app) => {
          if (createErr) {
            console.log(createErr);
            // validation error
            req.session.flash = {
              err: createErr,
              alert: {
                type: 'error',
                message: 'An error occoured.'
              }
            };
            req.session.input = req.params.all();
            return res.redirect('/apps/new');
          }

          // add the current user to this app
          Membership.create(
            { appId: app.id, userId: req.user.id },
            (membershipCreateErr, membership) => {
              if (membershipCreateErr) {
                req.session.flash = {
                  err: createErr,
                  alert: {
                    type: 'error',
                    message: 'An error occoured.'
                  }
                };
                req.session.input = req.params.all();
                return res.redirect('/apps/new');
              }

              req.session.flash = {
                alert: {
                  type: 'success',
                  message: 'App created successfully.'
                }
              };

              req.session.API_SECRET = plainApiSecret;
              return res.redirect(`/apps/${app.id}`);
            });
        });
      });
    });
  },

  show: (req, res) => {
    res.locals.app = req.currentApp;
    res.locals.apiSecret = null;
    if (req.session.API_SECRET) {
      res.locals.apiSecret = _.clone(req.session.API_SECRET);
      req.session.API_SECRET = null;
    }
    return res.view();
  },

  edit: (req, res) => {
    res.locals.app = req.currentApp;
    return res.view();
  },

  update: (req, res) => {
    const app = req.currentApp;
    app.name = req.param('name');
    app.description = req.param('description');
    app.performDeliveries = (req.param('perform_deliveries') === 'true');
    app.save((err) => {
      if (err) {
        // validation error
        req.session.flash = {
          err,
          alert: {
            type: 'error',
            message: 'An error occoured.'
        }};
        req.session.input = req.params.all();
        return res.redirect(`/apps/${app.id}/edit`);
      }

      req.session.flash = {
        alert: {
          type: 'success',
          message: 'App updated successfully.'
        }
      };
      return res.redirect(`/apps/${app.id}`);
    });
  },

  delete: (req, res) => {
    App.destroy({ id: req.param('id') }).exec((error) => {
      if (error) throw error;

      req.session.flash = {
        alert: {
          type: 'success',
          message: 'App deleted successfully.'
        }
      };

      res.redirect('/');
    });
  },

  // subscribe to the websocket callbacks
  subscribe: (req, res) => {
    if (!req.isSocket) {
      return res.badRequest();
    }

    sails.sockets.join(req.socket, 'app-updated');

    return res.ok();
  },

  reset: (req, res) => {
    const app = req.currentApp;

    generateApiSecret((plainApiSecret, hash) => {
      app.apiSecret = hash;
      req.session.API_SECRET = plainApiSecret;
      app.save((err) => {
        if (err) throw err;
        req.session.flash = {
          alert: {
            type: 'success',
            message: 'API secret reset successfully.'
          }
        };

        res.redirect(`/apps/${app.id}`)
      });
    });
  },
};
