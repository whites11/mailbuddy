/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const passport = require('passport');

module.exports = {
  index: (req, res) => {
    User.find().sort('id asc').exec((err, users) => {
      if (err) throw err;
      res.locals.users = users;
      res.view();
    });
  },

  new: (req, res) => {
    App.find({}).exec((err, apps) => {
      if (err) throw err;

      res.locals.availableApps = apps;

      res.view();
    });
  },

  create: (req, res) => {
    const params = {};
    const all = req.params.all();

    AuthService.generateResetPasswordToken({}, (err, token) => {
      if (err) throw err;

      params.email = all.email;
      params.isAdmin = (all.is_admin === 'true');
      params.resetPasswordToken = token;

      User.create(params, (err, user) => {
        if (err) {
          req.session.flash = {
            err,
            alert: {
              type: 'error',
              message: 'An error is preventing the creation of this user account.'
            }
          };
          req.session.input = req.params.all();

          return res.redirect('/admin/users/new');
        }

        // create memberships
        let newAppIds = req.param('apps');
        if (typeof newAppIds === 'string') {
          newAppIds = [newAppIds];
        }

        if (newAppIds) {
          Membership.create(
           newAppIds.map((appId) => {
             return {appId, userId: user.id};
           }),
           (membershipCreateErr, membership) => {
             if (membershipCreateErr) {
               throw membershipCreateErr;
             }

             AuthService.sendWelcomeEmail({ user }, (err) => {
               if (err) throw err;

               req.session.flash = {
                 alert: {
                   type: 'success',
                   message: 'User created successfully.'
                 }
               };

               return res.redirect('/admin/users');
             });
           });
        } else {
          // todo avoid this duplicated code
          AuthService.sendWelcomeEmail({ user }, (err) => {
            if (err) throw err;

            req.session.flash = {
              alert: {
                type: 'success',
                message: 'User created successfully.'
              }
            };

            return res.redirect('/admin/users');
          });
        }
      });
    });
  },

  edit: (req, res) => {
    User.findOne(req.param('id')).populate('apps').exec((err, user) => {
      if (err) throw err;

      if (user) {
        res.locals.user = user;
        App.find({}).exec((err, apps) => {
          if (err) throw err;

          const selectedAppIds = user.apps.map((item) => {
            return item.id;
          });

          res.locals.availableApps = apps.map((item) => {
            item.active = _.includes(selectedAppIds, item.id);
            return item;
          });

          res.view();
        });
      } else {
        res.notFound();
      }
    });
  },

  update: (req, res) => {
    User.findOne(req.param('id')).populate('memberships').exec((err, user) => {
      if (err) throw err;

      let newAppIds = req.param('apps');
      if (typeof newAppIds === 'string') {
        newAppIds = [newAppIds];
      }

      Membership.destroy({ userId: user.id }).exec((err) => {
        if (err) throw err;

        if (newAppIds) {
          Membership.create(
            newAppIds.map((appId) => {
              return {appId, userId: user.id};
            }),
            (membershipCreateErr, membership) => {
              if (membershipCreateErr) {
                throw membershipCreateErr;
              }

              req.session.flash = {
                alert: {
                  type: 'success',
                  message: 'User updated successfully.'
                }
              };

              res.redirect("/admin/users/");
            });
        } else {
          req.session.flash = {
            alert: {
              type: 'success',
              message: 'User updated successfully.'
            }
          };

          res.redirect("/admin/users/");
        }
      });
    });
  },

  delete: (req, res) => {
    if (req.user.id !== parseInt(req.param('id'))) {
      User.destroy({ id: req.param('id') }).exec((error) => {
        if (error) throw error;

        req.session.flash = {
          alert: {
            type: 'success',
            message: 'User deleted successfully.'
          }
        };
        res.redirect('/admin/users');
      });
    } else {
      res.forbidden();
    }
  },

  admin: (req, res) => {
    if (req.user.id !== parseInt(req.param('id'))) {
      User.findOne(req.param('id')).exec((err, user) => {
        if (err) throw err;

        if (user) {
          user.isAdmin = !user.isAdmin;
          user.save((saveErr) => {
            if (saveErr) { throw saveErr; }

            req.session.flash = {
              alert: {
                type: 'success',
                message: 'User updated successfully.'
              }
            };

            res.redirect('/admin/users');
          });
        } else {
          res.notFound();
        }
      });
    } else {
      res.forbidden();
    }
  },

  invite: (req, res) => {
    AuthService.sendWelcomeEmail({ user: req.selectedUser }, (err) => {
      if (err) throw err;

      req.session.flash = {
        alert: {
          type: 'success',
          message: 'Invitation sent correctly.'
        }
      };

      return res.redirect('/admin/users');
    });
  },

  passwordForm: (req, res) => {
    return res.view();
  },

  updatePassword: (req, res) => {
    // check if the currentPassword entered is correct
    passport.authenticate('local', (err, user, info) => {
      if ((err) || (!user)) {
        req.session.flash = {
          err: { invalidAttributes: { password: true }, Errors: { password: [{ message: "The current password doesn't match our records."}] }},
          alert: {
            type: 'error',
            message: 'An error occoured.'
          }
        };
        req.session.input = req.params.all();
        return res.redirect('/users/password');
      }

      const newPassword = req.param('new_password');
      //todo this is hacky, but I can't find a way to validate virtual attributes on the model
      if (newPassword.length < 8) {
        req.session.flash = {
          err: { invalidAttributes: { newPassword: true }, Errors: { newPassword: [{ message: "Please enter a password at least 8 characters long."}] }},
          alert: {
            type: 'error',
            message: 'An error occoured.'
          }
        };
        req.session.input = req.params.all();
        return res.redirect('/users/password');
      }

      // use is ok, currentPassword matches, update with the new password.
      req.user.newPassword = newPassword;
      req.user.save((err) => {
        if (err) {
          req.session.flash = {
            alert: {
              type: 'error',
              message: 'An error occoured.'
            }
          };
          return res.redirect('/users/password');
        }

        req.session.flash = {
          alert: {
            type: 'success',
            message: 'Password changed successfully.'
          }
        };

        return res.redirect('/');
      });
    })(req, res);
  }
};
