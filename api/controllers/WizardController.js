/**
 * WizardController
 *
 * @description :: First configuration "wizard"
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  new: (req, res) => {
    User.count().exec((err, count) => {
      if (err) throw err;
      if (count > 0) {
        return res.redirect('/');
      }
      return res.view('wizard/new', { layout: 'simple' });
    });
  },

  create: (req, res) => {
    const params = {};
    const all = req.params.all();
    params.email = all.email;
    params.password = all.password;
    params.isAdmin = true;

    User.create(params, (err, user) => {
      if (err) {
        req.session.flash = { err };
        req.session.input = req.params.all();
        return res.redirect('/wizard');
      }

      // create a new app to allow sending emails for mailbuddy
      App.create({
        name: 'Mailbuddy',
        description: 'This is the internal mailbuddy app.',
        apiKey: 'MAILBUDDY',
        apiSecret: 'MAILBUDDY',
        isSystem: true,
        emailsCount: 0,
        performDeliveries: true
      }, (err, app) => {
        if (err) {
          // todo handle error
        }

        // add the first user to the new app
        Membership.create({
          userId: user.id,
          appId: app.id
        }, (err, membership) => {
          if (err) {
            // todo handle error
          }
        });
      });

      req.logIn(user, (loginErr) => {
        if (loginErr) {
          res.send(loginErr);
        }
        return res.redirect('/');
      });
    });
  },
};

