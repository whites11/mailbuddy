/**
 * LogsController
 *
 * @description :: Server-side logic for managing emails
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  create: (req, res) => {
    const params = req.params.all();
    let recipient = params.content.match(/to=<([^>]*)>/);
    let status = params.content.match(/status=([^ ]*)/);
    if (!recipient || !status) {
      res.send(JSON.stringify({ status: "invalid_log" }));
      return;
    }

    recipient = recipient[1];
    status = status[1];

    if (params.queue_id && params.queue_id !== "") {

      // find an email with this queue id
      Email.findOne({ queueId: params.queue_id })
        .then((email) => {
          if (email) {

            // find a previous log entry for this queue_id and recipient
            return Log.findOne({queueId: params.queue_id, recipient});
          }

          // email not found, just ignore the log
          res.send(JSON.stringify({ status: "ignored" }));
          throw new Error();
        }).then((previousLog) => {
          if (previousLog) {
            // previous log found, update it with latest content
            previousLog.content = params.content;
            previousLog.status = status;
            previousLog.timestamp = sails.moment(params["@timestamp"]).toDate();

            previousLog.save((err) => {
              if (err) {
                res.status(500).send();
              } else {
                res.send(JSON.stringify({ status: "ok" }));
              }
            });

          } else {
            Log.create({
              queueId: params.queue_id,
              program: params.program,
              content: params.content,
              recipient,
              status,
              timestamp: sails.moment(params["@timestamp"]).toDate()
            }, (createErr, log) => {
              if (createErr) {
                res.status(500).send();
              }
              res.send(JSON.stringify({ status: "ok" }));
            });
          }
        })
	.catch((err) => {
	});
    } else {

      // successful response, it is not useful to make logstash retry sending this event
      res.send(JSON.stringify({ status: "invalid_log" }));
    }
  },
};
