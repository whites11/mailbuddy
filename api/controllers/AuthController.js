/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const passport = require('passport');

module.exports = {
  _config: {
    actions: false,
    shortcuts: false,
    rest: false,
  },

  loginForm: (req, res) => {
    // check if there is an existing user
    User.count().exec((err, count) => {
      if (err) throw err;

      if (count > 0) {
        return res.view('auth/loginform', { layout: 'simple' });
      } else {
        res.redirect('/wizard');
      }
    });
  },

  login: (req, res) => {
    passport.authenticate('local', (err, user, info) => {
      if ((err) || (!user)) {
        req.session.flash = { err: info.message };
        req.session.input = req.params.all();
        return res.redirect('/login');
      }
      req.logIn(user, (loginErr) => {
        if (loginErr) {
          req.session.flash = { err: loginErr };
          req.session.input = req.params.all();
          return res.redirect('/login');
        }
        return res.redirect('/');
      });
    })(req, res);
  },

  logout: (req, res) => {
    req.logout();
    res.redirect('/');
  },

  resetForm: (req, res) => {
    res.view('auth/reset', { layout: 'simple' });
  },

  reset: (req, res) => {
    User.findOne({ email: req.param('email') }, (err, user) => {
      if (err) throw err;
      if (user) {
        AuthService.generateResetPasswordToken({}, (err, token) => {
          user.resetPasswordToken = token;
          user.save((err) => {
            if (err) throw err;

            AuthService.sendResetPasswordInstructions({ user }, (err) => {
              if (err) throw err;

              res.redirect("/login");
            });

          });
        });
      }
    });
  },

  passwordForm: (req, res) => {
    res.locals.resetPasswordToken = req.param('token');
    res.view('auth/passwordform', { layout: 'simple' });
  },

  password: (req, res) => {
    const token = req.param('reset_password_token');
    User.findOne({ resetPasswordToken: token }, (err, user) => {
      if (err) throw err;

      if (user) {
        // token was valid
        const newPassword = req.param('new_password');
        //todo this is hacky, but I can't find a way to validate virtual attributes on the model
        if (newPassword.length < 8) {
          req.session.flash = {err: { invalidAttributes: { newPassword: true }, Errors: { newPassword: [{ message: "Please enter a password at least 8 characters long."}] }}};
          req.session.input = req.params.all();
          return res.redirect(`/password?token=${token}`);
        }

        // use is ok, currentPassword matches, update with the new password.
        user.newPassword = newPassword;
        user.resetPasswordToken = null;
        user.save((err) => {
          if (err) {
            req.session.flash = { err: true };
            return res.redirect(`/password?token=${token}`);
          }

          return res.redirect('/login');
        });

      } else {
        // invalid token
        req.session.flash = { err: true };
        return res.redirect(`/password?token=${token}`);
      }
    });
  },
};

