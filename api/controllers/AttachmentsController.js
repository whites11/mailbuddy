/**
 * AttachmentsController
 *
 * @description :: First configuration "wizard"
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const path = require('path');

module.exports = {
  download: (req, res) => {
    const filePath = path.resolve(
      'attachments/',
      req.currentAttachment.id.toString(),
      req.currentAttachment.filename
    );

    return res.download(filePath);
  },
};
