/**
 * EmailsController
 *
 * @description :: Server-side logic for managing emails
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const prettyBytes = require('pretty-bytes');
const rimraf = require('rimraf');

module.exports = {
  /**
   * `EmailsController.index()`
   */
  index: (req, res) => {
    const page = req.param('page') || 1;
    const perPage = 20;
    res.locals.page = parseInt(page);
    res.locals.perPage = perPage;
    Email.count({ app_id: req.currentApp.id }, (err, count) => {
      res.locals.totPages = Math.floor(count / perPage) + 1;

      Email.find({ where: { app_id: req.currentApp.id }, sort: 'id desc' }).paginate({page: page, limit: perPage}).then((emails) => {
        res.locals.emails = emails;
        res.locals.currentApp = req.currentApp;

        return res.view();
      });
    });
  },

  // subscribe to the websocket callbacks
  subscribe: (req, res) => {
    if (!req.isSocket) {
      return res.badRequest();
    }

    sails.sockets.join(req.socket, 'email-' + req.currentApp.id);

    return res.ok();
  },

  /**
   * `EmailsController.show()`
   */
  show: (req, res) => {
    res.locals.email = req.currentEmail;
    res.locals.prettyBytes = prettyBytes;
    return res.view('emails/show', { layout: null });
  },

  delete: (req, res) => {
    req.currentEmail.destroy((err) => {
      if (err) throw err;

      // delete any email attachments
      req.currentEmail.attachments.forEach((attachment) => {
        rimraf(`${__dirname}/../../attachments/${attachment.id}`, () => {});
      });

      req.session.flash = {
        alert: {
          type: 'success',
          message: 'Email deleted successfully.'
        }
      };

      res.redirect(`/apps/${req.currentEmail.appId}/emails`);
    });
  },
};

