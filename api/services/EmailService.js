// api/services/EmailService.js
module.exports = {

  enqueueEmail: function (options, done) {

    if (options.mail.cc === undefined) {
      options.mail.cc = "";
    }

    if (options.mail.bcc === undefined) {
      options.mail.bcc = "";
    }

    if (options.mail.from === undefined) {
      options.mail.from = 'no-reply@mailbuddy.com';
    }

    Email.create(
      {
        app_id: options.app.id,
        from: options.mail.from,
        messageId: `pwd-recovery-${Math.floor(new Date() / 1000)}`,
        to: options.mail.to,
        cc: options.mail.cc,
        bcc: options.mail.bcc,
        subject: options.mail.subject,
        text: options.mail.text,
        html: options.mail.html,
        sentAt: new Date(),
        attachmentsCount: 0,
        sentCount: 0,
        errorsCount: 0,
        recipientsCount: 0
      },
      (err, email) => {
        done(err);
      }
    )
  }
};
