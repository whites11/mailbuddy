const rand = require('random-key');

module.exports = {
  sendResetPasswordInstructions: (options, done) => {

    const baseUrl = process.env['WEB_BASE_URL'];
    const url = `${baseUrl}/password?token=${options.user.resetPasswordToken}`;

    // get html template
    sails.hooks.views.render("email_templates/reset_password.html", { url }, function(err, html) {
      if (err) throw err;

      App.findOne({isSystem: true}).exec((err, app) => {
        if (err) throw err;

        if (app) {

          EmailService.enqueueEmail({
            app,
            mail: {
              from: process.env['MAIL_FROM'] || 'noreply@mailbuddy.com',
              to: options.user.email,
              subject: "[MailBuddy] Your reset password instructions",
              text: "",
              html: html
            }
          }, (err) => {
            done(err);
          });
        } else {
          throw 'Missing system app!';
        }
      });
    });
  },

  sendWelcomeEmail: (options, done) => {

    const baseUrl = process.env['WEB_BASE_URL'];
    const url = `${baseUrl}/password?token=${options.user.resetPasswordToken}`;

    // get html template
    sails.hooks.views.render("email_templates/welcome.html", { url }, function(err, html) {
      if (err) throw err;

      App.findOne({isSystem: true}).exec((err, app) => {
        if (err) throw err;

        if (app) {
          EmailService.enqueueEmail({
            app,
            mail: {
              from: process.env['MAIL_FROM'] || 'noreply@mailbuddy.com',
              to: options.user.email,
              subject: "[MailBuddy] Welcome to Mailbuddy",
              text: "",
              html: html
            }
          }, (err) => {
            done(err);
          });
        } else {
          throw 'Missing system app!';
        }
      });
    });
  },

  generateResetPasswordToken: (options, callback) => {
    // generate a random string
    const key = rand.generateBase30(40).toLowerCase();

    // ensure the random string is not currently an API key for any app
    User.findOne({ resetPasswordToken: key }).exec((err, record) => {
      if (err) {
        throw err;
      }

      if (record) {
        // a record was found, try again
        generateResetPasswordToken(callback);
      } else {
        // key is fine
        callback(null, key);
      }
    });
  }
};
