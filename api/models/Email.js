/**
 * Email.js
 *
 * @description :: Email model
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const forward = require('../../transporters/forward');

const updateEmailsCount = (email, cb) => {
  // update emailsCount in the app
  Email.count({ appId: email.appId }).exec((err, count) => {
    if (err) throw err;

    App.findOne({ id: email.appId }, (err, app) => {
      if (err) throw err;

      app.emailsCount = count;
      app.save((err) =>  {
        if (err) throw err;
        cb();
      });
    });
  });
};

module.exports = {

  tableName: 'emails',
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    appId: { type: 'integer', columnName: 'app_id' },
    app: { model: 'app', via: 'emails', columnName: 'app_id' },
    messageId: { type: 'string', columnName: 'message_id' },
    queueId: { type: 'string', columnName: 'queue_id' },
    from: { type: 'string' },
    to: { type: 'string' },
    cc: { type: 'string' },
    bcc: { type: 'string' },
    subject: { type: 'string' },
    text: { type: 'string' },
    html: { type: 'string' },
    sentAt: { type: 'datetime', columnName: 'sent_at' },
    forwarderResponse: { type: 'json', columnName: 'forwarder_response' },
    forwardedAt: { type: 'datetime', columnName: 'forwarded_at' },
    attachments: {
      collection: 'attachment',
      via: 'email',
    },
    attachmentsCount: { type: 'integer', columnName: 'attachments_count' },
    sentCount: { type: 'integer', columnName: 'sent_count' },
    errorsCount: { type: 'integer', columnName: 'errors_count' },
    recipientsCount: { type: 'integer', columnName: 'recipients_count' },
  },

  afterCreate: (email, cb) => {
    updateEmailsCount(email, () => {});
    // notify clients via websocket
    sails.sockets.broadcast(`email-${email.appId}`, email);

    App.findOne({ id: email.appId }).then((app) => {
      if (app.performDeliveries) {
        console.log(`Sending real email ${email.subject}`)
        // perform actual delivery
        forward(email).then(
          (info) => {
            // email successfully sent to the relay server
            const forwardedAt = new Date();
            const queueId = info.response.match(/queued as ([A-F0-9]+)/)[1];
            Email.update(
              { id: email.id },
              { forwarderResponse: info, forwardedAt: forwardedAt, queueId },
              (err) => {
                if (err) {
                  console.log(`Error persisting forwarderResponse: ${err}`);
                }
                email.forwarderResponse = info;
                email.forwardedAt = forwardedAt;
                email.queueId = queueId;

                sails.sockets.broadcast(`email-${email.appId}`, email);
              });
          },
          (error) => {
            console.log(error);
          });
      } else {
        console.log(`NOT sending real email ${email.subject}`)
      }
    });

    cb();
  },
  afterDestroy: (emails, cb) => { updateEmailsCount(emails[0], cb); },
};

