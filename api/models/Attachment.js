/**
 * App.js
 *
 * @description :: App model
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'attachments',
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    email: {
      model: 'email', via: 'attachments', columnName: 'email_id'
    },
    emailId: { type: 'integer', columnName: 'email_id' },
    contentType: { type: 'string', required: true, columnName: 'content_type' },
    filename: { type: 'string', required: true },
    checksum: { type: 'string', required: true },
    size: { type: 'integer' }
  },
};
