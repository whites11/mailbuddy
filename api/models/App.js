/**
 * App.js
 *
 * @description :: App model
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'apps',
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    name: { type: 'string', unique: true, required: true, alphanumericdashed: true },
    description: { type: 'string' },
    apiKey: { type: 'string', notNull: true, unique: true, columnName: 'api_key', required: true },
    apiSecret: { type: 'string', notNull: true, columnName: 'api_secret', required: true },
    emailsCount: { type: 'integer', columnName: 'emails_count' },
    performDeliveries: { type: 'boolean', columnName: 'perform_deliveries' },
    isSystem: { type: 'boolean', columnName: 'is_system' },
    memberships: { collection: 'membership', via: 'app' },
    users: {
      collection: 'user',
      through: 'membership',
    },
  },

  beforeValidate: (record, cb) => {
    if (!record.isSystem) {
      record.isSystem = false;
    }
    cb();
  },

  afterUpdate: (record, cb) => {
    sails.sockets.broadcast('app-updated', record);
    cb();
  },
};
