/**
 * Membership.js
 *
 * @description :: App-User membership
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'memberships',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    appId: { type: 'integer', columnName: 'app_id' },
    userId: { type: 'integer', columnName: 'user_id' },
    app: { model: 'app', via: 'memberships', columnName: 'app_id' },
    user: { model: 'user', via: 'memberships', columnName: 'user_id' },
  },
};
