/**
 * User.js
 *
 * @description :: User model
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const bcrypt = require('bcrypt');

const encryptPassword = (user, password, cb) => {
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(password, salt, (err, hash) => {
      if (err) {
        console.log(err);
        cb(err);
      } else {
        user.password = hash;
        cb();
      }
    });
  });
};

module.exports = {
  /*types: {
    matchConfirmation: function (password) {
      return password === this.confirm;
    }
  },*/

  tableName: 'users',
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    email: {
      type: 'email',
      required: true,
      unique: true,
    },
    password: {
      type: 'string',
      minLength: 8,
      required: false,
    },
    isAdmin: {
      type: 'boolean',
      columnName: 'is_admin',
    },
    resetPasswordToken: {
      type: 'string',
      columnName: 'reset_password_token'
    },
    memberships: { collection: 'membership', via: 'user' },
    apps: {
      collection: 'app',
      through: 'membership',
    },
  },
  beforeValidate: (values, next) => {
    // perform user sanitization only on create
    if (values.id === undefined) {
      if (values.email) {
        values.email = values.email.toLowerCase();
      }
    }
    next();
  },
  beforeCreate: (user, cb) => {
    if (user.password !== undefined) {
      encryptPassword(user, user.password, cb);
    } else {
      cb();
    }
  },
  beforeUpdate: (user, cb) => {
    if (user.newPassword !== undefined) {
      encryptPassword(user, user.newPassword, cb);
    } else {
      cb();
    }
  },
};

