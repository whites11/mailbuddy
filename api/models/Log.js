/**
 * App.js
 *
 * @description :: App model
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const updateSentCount = (log, cb) => {
  Email.findOne({ queueId: log.queueId }).then((email) => {
    if (email) {
      // find logs for this email where "status == sent"
      Log.count({ queueId: email.queueId, status: 'sent' }).exec((error, sent) => {
        email.sentCount = sent;

        Log.count({ queueId: email.queueId }).exec((error, all) => {
          email.errorsCount = (all - sent);
          email.save((err) => {
            if (err) {
              console.log("Error saving email stats: ", err);
            }

            sails.sockets.broadcast(`email-${email.appId}`, email);
          });
        })
      });
    }
  });

  cb();
};

module.exports = {
  tableName: 'logs',
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {
    queueId: { type: 'string', columnName: 'queue_id' },
    program: { type: 'string', required: true, columnName: 'program' },
    content: { type: 'string', required: true },
    status: { type: 'string', required: true },
    recipient: { type: 'string', required: true },
    timestamp: { type: 'datetime', required: true },
  },

  afterCreate: updateSentCount,
  afterUpdate: updateSentCount,
};
