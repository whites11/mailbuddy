$(document).ready(function() {
  $('[data-toggle="tooltip"]').tooltip();

  var elems = $('.js-switch');

  for (var i = 0; i < elems.length; i++) {
    var switchery = new Switchery(elems[i]);
  }

  sh_highlightDocument();
}).on('click', 'a[data-confirm]', function(event) {
    $this = $(this);

    event.preventDefault();
    event.stopImmediatePropagation();

    $this.data('confirmed', 'true');

    var msg = $this.data('confirm');
    bootbox.confirm(msg, function(result) {
      if (result) {
        document.location = $this.attr('href');
      }
    });
}).on('click', '[data-toggle]', function(event) {
  $target = $($(this).data('toggle'));

  $target.toggle();
}).on('click', 'tr[data-email_id]', function(event) {
  if ($(this).hasClass('info')) {
    $('.email_show_container').hide();
    $('tr.info[data-email_id]').removeClass('info');
  } else {
    var id = $(this).data('email_id');
    $('.email_show_container').hide();
    $('tr.info[data-email_id]').removeClass('info');
    $('#email_show_' + id).load('/emails/' + id);
    $('#email_show_container_' + id).show();
    $(this).addClass('info');
  }
});
