FROM node:9.8-slim

WORKDIR /usr/src/app

COPY package.json /usr/src/app
COPY package-lock.json /usr/src/app

RUN npm install --no-dev

COPY . /usr/src/app

CMD npm run start



