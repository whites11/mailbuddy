# mail-monitor
Simple node app that receives email through SMTP and displays them in a handy web UI

Run the development environment with:

`docker-compose up`

There is an official docker image if you want to give Mailbuddy a try!

https://hub.docker.com/r/whites11/mailbuddy/
