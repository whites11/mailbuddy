const passport = require('passport');
const bcrypt = require('bcrypt');
const LocalStrategy = require('passport-local').Strategy;

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findOne({ id }, (err, user) => {
    if (err || user === undefined) {
      done(err, null);
    } else {
      done(err, user);
    }
  });
});

passport.use(new LocalStrategy({ usernameField: 'email', passwordField: 'password' },
  (email, password, done) => {
    User.findOne({ email }, (err, user) => {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect email.' });
      }

      bcrypt.compare(password, user.password, (bcryptErr, res) => {
        if (!res) {
          return done(null, false, {
            message: 'Invalid Password',
          });
        }
        const returnUser = {
          email: user.email,
          createdAt: user.createdAt,
          id: user.id,
        };
        return done(null, returnUser, {
          message: 'Logged In Successfully',
        });
      });
    });
  }
));
