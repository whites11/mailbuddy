/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  'GET /apps/subscribe': 'AppsController.subscribe',

  'GET /': 'AppsController.index',
  'GET /apps/new': 'AppsController.new',
  'GET /apps/:id': 'AppsController.show',
  'GET /apps/:id/edit': 'AppsController.edit',
  'POST /apps/:id': 'AppsController.update',
  'POST /apps': 'AppsController.create',
  'GET /apps/:id/delete': 'AppsController.delete',
  'GET /apps/:id/reset': 'AppsController.reset',

  'GET /apps/:id/emails': 'EmailsController.index',
  'GET /emails/:id/subscribe': 'EmailsController.subscribe',
  'GET /emails/:id': 'EmailsController.show',
  'GET /emails/:id/delete': 'EmailsController.delete',

  'GET /attachments/:id/:filename': 'AttachmentsController.download',

  'GET /login': 'AuthController.loginForm',
  'POST /login': 'AuthController.login',
  'GET /logout': 'AuthController.logout',
  'GET /reset': 'AuthController.resetForm',
  'POST /reset': 'AuthController.reset',
  'GET /password': 'AuthController.passwordForm',
  'POST /password': 'AuthController.password',

  // user admin routes
  'GET /users/new': 'UsersController.new',
  'POST /users': 'UsersController.create',
  'GET /users/password': 'UsersController.passwordForm',
  'POST /users/password': 'UsersController.updatePassword',

  // first configuration wizard
  'GET /wizard': 'WizardController.new',
  'POST /wizard': 'WizardController.create',

  // ADMIN routes
  'GET /admin/users': 'UsersController.index',
  'GET /admin/users/new': 'UsersController.new',
  'GET /admin/users/:id/edit': 'UsersController.edit',
  'POST /admin/users/:id': 'UsersController.update',
  'GET /admin/users/:id/delete': 'UsersController.delete',
  'GET /admin/users/:id/admin': 'UsersController.admin',
  'GET /admin/users/:id/invite': 'UsersController.invite',

  // API routes
  'POST /api/logs': 'LogsController.create',

};
