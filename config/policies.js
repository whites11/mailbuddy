/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */

module.exports.policies = {
  // the catchall policy is restrictive, to avoid that a missing
  // policy setup leaves too many privileges to non members
  '*': ['ensureCurrentUser', 'isAuthenticated', 'ensureAdmin', 'flash'],

  AppsController: {
    '*': ['ensureCurrentUser', 'isAuthenticated', 'flash'],
    new: ['ensureCurrentUser', 'isAuthenticated', 'ensureAdmin', 'flash'],
    create: ['ensureCurrentUser', 'isAuthenticated', 'ensureAdmin', 'flash'],
    edit: ['ensureCurrentUser', 'isAuthenticated', 'ensureAdmin', 'loadAndAuthorizeApp', 'ensureIsNotSystemApp', 'flash'],
    update: ['ensureCurrentUser', 'isAuthenticated', 'ensureAdmin', 'loadAndAuthorizeApp', 'ensureIsNotSystemApp', 'flash'],
    show: ['ensureCurrentUser', 'isAuthenticated', 'loadAndAuthorizeApp', 'flash'],
    delete: ['ensureCurrentUser', 'isAuthenticated', 'ensureAdmin', 'loadAndAuthorizeApp', 'ensureIsNotSystemApp', 'flash'],
    reset: ['ensureCurrentUser', 'isAuthenticated', 'ensureAdmin', 'loadAndAuthorizeApp', 'ensureIsNotSystemApp', 'flash'],
  },
  AttachmentsController: {
    download: ['ensureCurrentUser', 'isAuthenticated', 'loadAndAuthorizeAttachment', 'flash'],
  },
  AuthController: {
    '*': ['ensureCurrentUser', 'flash'],
  },
  EmailsController: {
    '*': ['ensureCurrentUser', 'isAuthenticated', 'flash'],
    index: ['ensureCurrentUser', 'isAuthenticated', 'loadAndAuthorizeApp', 'flash'],
    show: ['ensureCurrentUser', 'isAuthenticated', 'loadAndAuthorizeEmail', 'flash'],
    delete: ['ensureCurrentUser', 'isAuthenticated', 'loadAndAuthorizeEmail', 'flash'],
    subscribe: ['isAuthenticated', 'loadAndAuthorizeApp'],
  },
  LogsController: {
    '*': [],
  },
  UsersController: {
    '*': ['ensureCurrentUser', 'isAuthenticated', 'ensureAdmin', 'flash'],
    passwordForm: ['ensureCurrentUser', 'isAuthenticated', 'flash'],
    updatePassword: ['ensureCurrentUser', 'isAuthenticated', 'flash'],
    invite: ['ensureCurrentUser', 'isAuthenticated', 'ensureAdmin', 'loadUser', 'flash'],
  },
  WizardController: {
    '*': ['ensureCurrentUser', 'flash'],
  },
};
