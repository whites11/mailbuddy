/**
 * Session Configuration
 * (sails.config.session)
 *
 * Sails session integration leans heavily on the great work already done by
 * Express, but also unifies Socket.io with the Connect session store. It uses
 * Connect's cookie parser to normalize configuration differences between Express
 * and Socket.io and hooks into Sails' middleware interpreter to allow you to access
 * and auto-save to `req.session` with Socket.io the same way you would with Express.
 *
 * For more information on configuring the session, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.session.html
 */

const session = {
  secret: '14064e773d2f561e7fe0c332d841ad76',
};

if (process.env.REDIS_HOST && process.env.REDIS_PORT) {
  session.adapter = 'redis';
  session.host = process.env.REDIS_HOST;
  session.port = process.env.REDIS_PORT;
  // ttl: <redis session TTL in seconds>,
  // db: 0,
  // pass: <redis auth password>,
  // prefix: 'sess:',
}

module.exports.session = session;
