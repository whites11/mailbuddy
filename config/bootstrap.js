/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

const SMTPServer = require('smtp-server').SMTPServer;
const simpleParser = require('mailparser').simpleParser;
const bcrypt = require('bcrypt');
const fs = require('fs');

module.exports.bootstrap = function(cb) {

  sails.moment = require('moment');

  const keyPath = process.env.TLS_KEY_PATH;
  const certPath = process.env.TLS_CERT_PATH;

  const options = {
    secure: false,
    authOptional: false,
    ignoreTLS: true,
    hideSTARTTLS: true,
    onAuth: (auth, session, callback) => {
      // todo review error handling

      App.findOne({ apiKey: auth.username }).then((app) => {
        bcrypt.compare(auth.password, app.apiSecret, (err, res) => {
          if (err || !res) {
            callback(
              {
                message: 'Unauthorized',
              },
              {
                user: null,
              });
          } else {
            callback(
              null,
              {
                user: { appId: app.id },
              });
          }
        });
      }).catch((err) => {
        console.log(err);
        callback(
          {
            message: 'Unauthorized',
          },
          {
            user: null,
          });
      });
    },
    onData: (stream, session, callback) => {
      const appId = session.user.appId;
      simpleParser(stream).then(
        (mail) => {
          Email.create({
            app_id: appId,
            from: mail.from.text,
            messageId: mail.messageId,
            to: mail.to.text,
            cc: mail.to.text,
            bcc: mail.to.text,
            subject: mail.subject,
            text: mail.text,
            html: mail.html,
            sentAt: mail.date,
            attachmentsCount: mail.attachments.length,
            recipientsCount: mail.to.value.length,
            sentCount: 0,
            errorsCount: 0
          }).then((email) => {

            if (mail.attachments.length > 0) {
              // create attachments
              let requests = mail.attachments.map((attachment) => {
                return new Promise((resolve) => {
                  Attachment.create({
                    emailId: email.id,
                    contentType: attachment.contentType,
                    filename: attachment.filename,
                    checksum: attachment.checksum,
                    size: attachment.size
                  }).then((attach) => {
                    // persist file on filesystem
                    const dir = __dirname + `/../attachments/${attach.id}`;
                    fs.mkdir(dir, 0o755, (err) => {
                      if (err) {
                        console.log("Error creating directory: ", err);
                        // todo handle error
                        resolve();
                      } else {
                        fs.writeFile(`${dir}/${attach.filename}`, attachment.content, function (err) {
                          if (err) {
                            // todo handle error
                            console.log("Error persisting attachment: ", err);
                          }
                          resolve();
                        });
                      }
                    });

                  }).catch((err) => {
                    console.log("Error persisting attachment:" + JSON.stringify(err));
                    // TODO handle this error
                  });
                });
              });

              Promise.all(requests).then(() => callback(null, 'OK'));
            } else {
              callback(null, 'OK')
            }
          }).catch((err) => {
            console.log("Error persisting email:" + JSON.stringify(err));
            // TODO handle this error
            callback(null, 'OK');
          });
        }).catch(err => callback(err));
    },
  };

  if (keyPath && certPath) {
    if (fs.existsSync(keyPath) && fs.existsSync(certPath)) {
      console.log(`Setting UP TLS for SMTP receiver using ${keyPath} and ${certPath}`);
      options.key = fs.readFileSync(keyPath);
      options.cert = fs.readFileSync(certPath);
    } else {
      console.log(`${keyPath} or ${certPath} not found, skipping tls setup`);
    }
  } else {
    console.log(`TLS_KEY_PATH or TLS_CERT_PATH unset, skipping tls setup`);
  }

  // Start smtp server
  const smtp = new SMTPServer(options);

  smtp.listen(2525);

  cb();
};
