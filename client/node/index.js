const nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
  host: '127.0.0.1',
  port: '2525',
  //tls: {
  //  // do not fail on invalid certs
  //  rejectUnauthorized: false
  //},
  //secure: true,
  //ignoreTLS: true,
  //requireTLS: true,
  auth: {
    user: "C2NM0NMV34",
    pass: "L63LYUPCHP",
  }
});

// setup email data with unicode symbols
const mailOptions = {
  from: '"Fred Foo 👻" <foo@blurdybloop.com>', // sender address
  to: 'whites11@gmail.com', // list of receivers
  subject: 'Hello ✔', // Subject line
  text: 'this is the text version', // plain text body
  html: '<b>This is the HTML version</b>', // html body
  cc: 'whites11@gmail.com',
  bcc: 'whites11@gmail.com',
  attachments: [
     {
      path: __dirname + '/attachment.png'
     }
  ]
};

// send mail with defined transport object
transporter.sendMail(mailOptions, (error, info) => {
  if (error) {
    return console.log(error);
  }
  console.log('Message %s sent: %s', info.messageId, info.response);
});

