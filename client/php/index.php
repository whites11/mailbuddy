<?php

require_once __DIR__ . '/vendor/autoload.php';

// Create the Transport
$transport = (new Swift_SmtpTransport('localhost', 2525, 'tls'))
    ->setUsername('M13DUYCMCS')
    ->setPassword('6N3HT3218Q')
    ->setStreamOptions(array('ssl' => array('allow_self_signed' => true, 'verify_peer' => false)));
;

// Create the Mailer using your created Transport
$mailer = new Swift_Mailer($transport);

// Create a message
$message = (new Swift_Message('Wonderful Subject'))
    ->setFrom(['john@doe.com' => 'John Doe'])
    ->setTo(['receiver@domain.org', 'other@domain.org' => 'A name'])
    ->setBody('Here is the message itself')
;

// Send the message
$result = $mailer->send($message);
