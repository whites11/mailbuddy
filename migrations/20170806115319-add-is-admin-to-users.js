'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'users';
const COLUMN_NAME = 'is_admin';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = (options, seedLink) => {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.addColumn(TABLE_NAME, COLUMN_NAME, { type: 'boolean', notNull: true, defaultValue: false }, callback);
};

exports.down = (db, callback) => {
  db.removeColumn(TABLE_NAME, COLUMN_NAME, callback);
};

exports._meta = {
  'version': 1,
};
