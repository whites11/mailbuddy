'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'emails';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.addColumn(TABLE_NAME, 'queue_id', { type: 'string', notNull: false, default: false }, callback);
};

exports.down = (db, callback) => {
  db.removeColumn(TABLE_NAME, 'queue_id', callback);
};

exports._meta = {
  version: 1,
};
