'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'attachments';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = (options, seedLink) => {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.createTable(TABLE_NAME, {
    id: { type: 'integer', primaryKey: true, autoIncrement: true },
    email_id: { type: 'integer', notNull: true },
    content_type: { type: 'string', notNull: true },
    filename: { type: 'string', notNull: true },
    checksum: { type: 'string', notNull: true },
    size: { type: 'integer', notNull: true },
  }, () => {
    db.addForeignKey(TABLE_NAME, 'emails', 'emails_fk',
      {
        email_id: 'id',
      },
      {
        onDelete: 'CASCADE',
        onUpdate: 'RESTRICT',
      }, callback);
  });
};

exports.down = (db, callback) => {
  db.dropTable(TABLE_NAME, callback);
};

exports._meta = {
  version: 1,
};
