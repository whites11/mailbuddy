'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'emails';
const COLUMN_NAME = 'app_id';

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = (options, seedLink) => {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.addColumn(TABLE_NAME, COLUMN_NAME, { type: 'int', notNull: true, defaultValue: 0 }, callback);
};

exports.down = (db, callback) => {
  db.removeColumn(TABLE_NAME, COLUMN_NAME, callback);
};

exports._meta = {
  'version': 1,
};
