'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'apps';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.addColumn(TABLE_NAME, 'description', { type: 'text', notNull: false }, callback);
};

exports.down = (db, callback) => {
  db.removeColumn(TABLE_NAME, 'description', callback);
};

exports._meta = {
  version: 1,
};
