'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'emails';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.addColumn(TABLE_NAME, 'sent_count', { type: 'int', notNull: false, default: 0 }, () => {
    db.addColumn(TABLE_NAME, 'errors_count', { type: 'int', notNull: false, default: 0 }, () => {
      db.addColumn(TABLE_NAME, 'recipients_count', { type: 'int', notNull: false, default: 0 }, () => {
        db.runSql(`UPDATE ${TABLE_NAME} SET sent_count = 0, errors_count = 0, recipients_count = 0;`, () => {
          db.changeColumn(TABLE_NAME, 'sent_count', { type: 'int', notNull: true, default: 0 }, () => {
            db.changeColumn(TABLE_NAME, 'errors_count', { type: 'int', notNull: true, default: 0 }, () => {
              db.changeColumn(TABLE_NAME, 'recipients_count', { type: 'int', notNull: true, default: 0 }, callback);
            });
          });
        });
      });
  });
});
};

exports.down = (db, callback) => {
  db.removeColumn(TABLE_NAME, 'sent_count', () => {
    db.removeColumn(TABLE_NAME, 'errors_count', () => {
      db.removeColumn(TABLE_NAME, 'recipients_count', callback);
    });
  });
};

exports._meta = {
  version: 1,
};
