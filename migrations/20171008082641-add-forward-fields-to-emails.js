'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'emails';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.addColumn(TABLE_NAME, 'forwarder_response', { type: 'json', notNull: false }, () => {
    db.addColumn(TABLE_NAME, 'forwarded_at', { type: 'datetime', notNull: false }, callback);
  });
};

exports.down = (db, callback) => {
  db.removeColumn(TABLE_NAME, 'forwarder_response', () => {
    db.removeColumn(TABLE_NAME, 'forwarded_at', callback);
  });
};

exports._meta = {
  version: 1,
};
