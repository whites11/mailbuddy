'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'memberships';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = (options, seedLink) => {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.createTable(TABLE_NAME, {
    id: { type: 'integer', primaryKey: true, autoIncrement: true },
    app_id: { type: 'int', notNull: true, primaryKey: true },
    user_id: { type: 'int', notNull: true, primaryKey: true },
    created_at: { type: 'datetime', notNull: true, defaultValue: new String('now()') },
    updated_at: { type: 'datetime', notNull: true, defaultValue: new String('now()') },
  }, () => {
    db.addForeignKey(TABLE_NAME, 'apps', 'app_fk',
      {
        app_id: 'id',
      },
      {
        onDelete: 'CASCADE',
        onUpdate: 'RESTRICT',
      }, () => {
        db.addForeignKey(TABLE_NAME, 'users', 'user_fk',
          {
            user_id: 'id',
          },
          {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT',
          }, callback);
      });
  });
};

exports.down = (db, callback) => {
  db.dropTable(TABLE_NAME, callback);
};

exports._meta = {
  version: 1,
};
