'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'apps';
const COLUMN_NAME = 'emails_count';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = (options, seedLink) => {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.addColumn(TABLE_NAME, COLUMN_NAME, { type: 'integer', notNull: true, defaultValue: 0 }, () => {
    const sql = `update ${TABLE_NAME} set ${COLUMN_NAME} = (select count(*) from emails where app_id = ${TABLE_NAME}.id)`;
    db.runSql(sql, callback);
  });
};

exports.down = (db, callback) => {
  db.removeColumn(TABLE_NAME, COLUMN_NAME, callback);
};

exports._meta = {
  'version': 1,
};
