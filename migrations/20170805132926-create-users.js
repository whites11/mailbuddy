'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'users';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = (options, seedLink) => {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.createTable(TABLE_NAME, {
    id: { type: 'integer', primaryKey: true, autoIncrement: true },
    email: { type: 'string', notNull: true, unique: true },
    password: { type: 'string', notNull: true },
    created_at: { type: 'datetime', notNull: true, defaultValue: new String('now()') },
    updated_at: { type: 'datetime', notNull: true, defaultValue: new String('now()') },
  }, callback);
};

exports.down = (db, callback) => {
  db.dropTable(TABLE_NAME, callback);
};

exports._meta = {
  version: 1,
};
