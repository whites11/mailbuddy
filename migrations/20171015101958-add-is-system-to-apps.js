'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'apps';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.addColumn(TABLE_NAME, 'is_system', { type: 'boolean', notNull: false, default: false }, () => {
    db.runSql("update apps set is_system = 't';", () => {
      db.changeColumn(TABLE_NAME, 'is_system', { type: 'boolean', notNull: true, default: false }, callback);
    });
  });
};

exports.down = (db, callback) => {
  db.removeColumn(TABLE_NAME, 'is_system', callback);
};

exports._meta = {
  version: 1,
};
