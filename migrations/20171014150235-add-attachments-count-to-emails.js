'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'emails';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.addColumn(TABLE_NAME, 'attachments_count', { type: 'int', notNull: false, default: 0 }, () => {
    db.runSql("update emails set attachments_count = 0;", () => {
      db.changeColumn(TABLE_NAME, 'attachments_count', { type: 'int', notNull: true, default: 0 }, callback);
    });
  });
};

exports.down = (db, callback) => {
  db.removeColumn(TABLE_NAME, 'attachments_count', callback);
};

exports._meta = {
  version: 1,
};
