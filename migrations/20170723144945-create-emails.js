var dbm;
var type;
var seed;

const TABLE_NAME = 'emails';

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = (options, seedLink) => {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.createTable(TABLE_NAME, {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    message_id: { type: 'string', notNull:true, unique: true },
    from: 'text',
    to: 'text',
    subject: 'text',
    text: 'text',
    html: 'text',
    sent_at: 'datetime',
    created_at: { type: 'datetime', notNull: true, defaultValue: new String('now()') },
    updated_at: { type: 'datetime', notNull: true, defaultValue: new String('now()') },
  }, callback);
};

exports.down = (db, callback) => {
  db.dropTable(TABLE_NAME, callback);
};

exports._meta = {
  version: 1,
};
