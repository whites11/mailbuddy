'use strict';

var dbm;
var type;
var seed;

const TABLE_NAME = 'logs';

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = (options, seedLink) => {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = (db, callback) => {
  db.createTable(TABLE_NAME, {
    id: { type: 'integer', primaryKey: true, autoIncrement: true },
    queue_id: { type: 'string', notNull: true },
    program: { type: 'string', notNull: true },
    content: { type: 'string', notNull: true },
    status: { type: 'string', notNull: true },
    recipient: { type: 'string', notNull: true },
    timestamp: 'datetime',
  }, callback);
};

exports.down = (db, callback) => {
  db.dropTable(TABLE_NAME, callback);
};

exports._meta = {
  version: 1,
};
