const nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
  host: process.env.RELAY_HOST,
  port: process.env.RELAY_PORT,
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false,
  },
});

// transporter.verify((error, success) => {
//   if (error) {
//     console.log(error);
//     process.exit(1);
//   } else {
//     console.log('Server is ready to take our messages');
//   }
// });

module.exports = (mail) => {
  return new Promise((resolve, reject) => {
    // setup email data with unicode symbols
    const mailOptions = {
      from: mail.from,
      to: mail.to,
      subject: mail.subject,
      text: mail.text,
      html: mail.html,
      cc: mail.cc,
      bcc: mail.bcc,
      attachments: [],
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        reject(error);
      } else {
        resolve(info);
      }
    });
  });
};
