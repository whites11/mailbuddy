#!/bin/bash

set -e

cd /usr/src/app
npm install

# run migrations
node_modules/.bin/db-migrate up

if [ "$ENV" == "dev" ]
then
  npm run dev
else
  npm run start
fi

