#!/bin/bash
cp /main.cf /etc/postfix/main.cf
/bin/echo "Adding the domain ${1} to postfix main.cf config.";
echo "myhostname = ${1}" >> /etc/postfix/main.cf
touch /logs/mail.log
chmod 777 /logs/mail.log
service rsyslog start;
service postfix start;
sleep 20;
tail -f /logs/mail.log
